# SinglePageRouter

Router for single pages in Javascript.

## System requirements

Using docker:

- `Docker version 18.09.2`
- `docker-compose version 1.23.2`

Using node:

- `NodeJs version 10.16.3`

## Install the project

Using docker:

- `docker-compose up --build`

> It runs the tests after complete de installation.

Using node:

- `npm install`

## Run tests

After complete the installation, you have to run:

Using docker:

- `docker-compose run --rm npm test`

Using node.

- `npm test`
