FROM node:10.16.3-stretch-slim

WORKDIR /opt/router

COPY package* ./
RUN npm install
COPY . .
