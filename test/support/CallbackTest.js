
class CallbackTest {
  constructor() {
    this.hasExecuted = false

    this.execute = this.execute.bind(this)
  }

  execute() {
    this.hasExecuted = true
  }

  hasBeenExecuted() {
    return this.hasExecuted
  }
}

module.exports = CallbackTest
