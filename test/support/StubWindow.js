
const NO_HASH = ""
const ROOT = '/'

class StubWindow {
  constructor() {
    this.location = {
      hash: NO_HASH
    }

    this.listeners = {}
  }

  addEventListener(event, callback) {
    this.listeners[event] = callback
  }

  removeEventListener(_) {}

  join(path) {
    this.storeHash(path)

    this.listeners.load()
  }

  back(path) {
    this.storeHash(path)

    this.listeners.popstatus()
  }

  change(path) {
    this.storeHash(path)

    this.listeners.hashchange()
  }

  //private

  storeHash(path) {
    this.location.hash = NO_HASH

    if (!this.isRoot(path)) {
      this.location.hash = this.toHash(path)
    }
  }

  isRoot(path) {
    return (path === ROOT)
  }

  toHash(path) {
    const hash = path.substr(1)

    return hash
  }
}

module.exports = StubWindow
