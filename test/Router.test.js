const CallbackTest = require('./support/CallbackTest')
const StubWindow = require('./support/StubWindow')
const Router = require('../src/Router')
const { expect } = require('chai')

describe('Router', () => {
  let myWindow = undefined
  let callback = undefined
  let router = undefined
  const ROOT = '/'
  const HASH = '/#'
  const A_PATH = '/aPath'

  beforeEach(() => {
    myWindow = new StubWindow()
    callback = new CallbackTest()
    router = new Router(myWindow)
  })

  it('executes a callback when join to root', () => {
    router.add(ROOT, callback.execute)

    myWindow.join(ROOT)

    expect(callback.hasBeenExecuted()).to.be.true
  })

  it('executes a callback when join to some path with hash', () => {
    router.add(A_PATH, callback.execute)

    myWindow.join(HASH + A_PATH)

    expect(callback.hasBeenExecuted()).to.be.true
  })

  it('executes a callback when move back to some path with hash', () => {
    router.add(A_PATH, callback.execute)

    myWindow.back(HASH + A_PATH)

    expect(callback.hasBeenExecuted()).to.be.true
  })

  it('executes a callback when changes to some path with hash', () => {
    router.add(A_PATH, callback.execute)

    myWindow.change(HASH + A_PATH)

    expect(callback.hasBeenExecuted()).to.be.true
  })

  it('executes a callback when join to unextstent path with hash', () => {
    router.notFoundPath(callback.execute)

    myWindow.join('/#/unexistentPath')

    expect(callback.hasBeenExecuted()).to.be.true
  })
})
