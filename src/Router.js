
const NOT_DECLARED = undefined
const ROOT_ROUTE = '/'
const ROOT_HASH = ''
const EVENTS = ['load', 'popstatus', 'hashchange']

class Router {
  static prepare() {
    const router = new Router(window)

    return router
  }

  constructor(win) {
    this.window = win
    this.addEventListeners()

    this.notFound = NOT_DECLARED
    this.hashes = {}
  }

  add(rawRoute, callback) {
    const hash = this.toHash(rawRoute)

    this.hashes[hash] = callback
  }

  notFoundPath(callback) {
    this.notFound = callback
  }

  //private

  executeHashesCallback() {
    const callback = this.callbackForCurrenRoute()

    this.execute(callback)
  }

  addEventListeners() {
    EVENTS.forEach((event) => {
      this.addEventListener(event)
    })
  }

  addEventListener(event) {
    const callback = this.executeHashesCallback.bind(this)

    this.window.removeEventListener(event, callback)
    this.window.addEventListener(event, callback)
  }

  toHash(route) {
    let hash = ROOT_HASH

    if(!this.isRoot(route)) { hash = '#' + route }

    return hash
  }

  isRoot(route) {
    return (route === ROOT_ROUTE)
  }

  callbackForCurrenRoute() {
    const route = this.currentRoute()

    return this.callbackFor(route)
  }

  callbackFor(route) {
    let callback = this.hashes[route]
    if (!callback) { callback = this.notFound }

    return callback
  }

  currentRoute() {
    return this.window.location.hash
  }

  execute(callback) {
    callback()
  }
}

module.exports = Router
